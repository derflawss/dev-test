﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Model = BauerMediaau.Model;
using Data = Bauermediaau.Data;

namespace Business
{
    public class RestaurantService : IRestaurantService
    {
        private Data.IRestaurantRepository _repository;

        public RestaurantService() : this(new Data.RestaurantRepository()) { }

        public RestaurantService(Data.IRestaurantRepository repository)
        {
            if (repository == null)
                throw new ArgumentNullException();

            Mapper.CreateMap<Data.Restaurant, Model.Restaurant>();
            Mapper.CreateMap<Model.Restaurant, Data.Restaurant>();

            _repository = repository;
        }

        public IQueryable<Model.Restaurant> GetAll()
        {
            var ret = _repository.GetAll();

            var transformed = Mapper.Map<List<Model.Restaurant>>(ret);

            return transformed.AsQueryable();
        }

        public Model.Restaurant GetById(int id)
        {
            var ret = _repository.GetById(id);

            var transformed = Mapper.Map<Model.Restaurant>(ret);

            return transformed;

        }

        public void Update(Model.Restaurant restaurant)
        {
            if (restaurant == null)
                throw new ArgumentNullException();

            var transformed = Mapper.Map<Data.Restaurant>(restaurant);

            //TODO: Running out of time to do stored procedure
            var repo = new Data.RestaurantRepository();

            repo.Update(transformed);

            repo.SaveChanges();

            repo = null;
        }

    }
}
