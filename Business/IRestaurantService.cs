﻿using System.Linq;
using BauerMediaau.Model;

namespace Business
{
    public interface IRestaurantService
    {
        IQueryable<Restaurant> GetAll();
        Restaurant GetById(int id);
        void Update(Restaurant restaurant);
    }
}
