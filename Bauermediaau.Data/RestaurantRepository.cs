﻿using System;

namespace Bauermediaau.Data
{
    public class RestaurantRepository : RepositoryBase<Restaurant>, IRestaurantRepository
    {
        public RestaurantRepository()
            : base(new DatabaseEntities()) { }

        public RestaurantRepository(DatabaseEntities context)
            : base(context){ }

    }
}
