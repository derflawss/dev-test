﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace Bauermediaau.Data
{
    public class RepositoryBase<T> : IRepository<T> where T : class
    {
        protected DbSet<T> DatabaseSet { get; set; }
        protected DbContext Context { get; set; }

        public RepositoryBase(DbContext context)
        {
            if (context == null)
            {
                throw new ArgumentException("An instance of DbContext is required to this repository.", "context");
            }

            Context = context;
            DatabaseSet = Context.Set<T>();
        }

        public IQueryable<T> GetAll()
        {
            return DatabaseSet;
        }

        public T GetById(int id)
        {
            return DatabaseSet.Find(id);
        }

        public void Add(T entity)
        {
            DbEntityEntry entry = Context.Entry(entity);
            if (entry.State != EntityState.Detached)
            {
                entry.State = EntityState.Added;

            }
            else DatabaseSet.Add(entity);
        }

        public void Update(T entity)
        {
            DbEntityEntry entry = Context.Entry(entity);

            var key = GetKey(entry);

            if (entry.State == EntityState.Detached)
            {
                var currentEntry = DatabaseSet.Find(key);
                
                if (currentEntry != null)
                {
                    var attachedEntry = Context.Entry(currentEntry);
                    attachedEntry.CurrentValues.SetValues(entity);
                    return;
                }

                DatabaseSet.Attach(entity);

            }
            else
            {
                DatabaseSet.Add(entity);
            }

            entry.State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var entity = GetById(id);
            if (entity != null)
                Delete(entity);
        }

        public void Delete(T entity)
        {
            DbEntityEntry entry = Context.Entry(entity);
            if (entry.State != EntityState.Deleted)
            {
                entry.State = EntityState.Deleted;
            }
            else
            {
                DatabaseSet.Attach(entity);
                DatabaseSet.Remove(entity);
            }
        }

        public void Detach(T entity)
        {
            DbEntityEntry entry = Context.Entry(entity);
            entry.State = EntityState.Detached;
        }

        private int GetKey(DbEntityEntry entry)
        {
            var obj = entry.Entity;
            var prop =
                obj.GetType().GetProperties().FirstOrDefault(p => Attribute.IsDefined(p, typeof (KeyAttribute)));
            if (prop != null)
                return (int) prop.GetValue(obj, null);
            return 0;
        }


        public void SaveChanges()
        {
            Context.SaveChanges();
        }
    }
}
