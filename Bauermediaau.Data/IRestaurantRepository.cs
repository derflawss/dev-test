﻿namespace Bauermediaau.Data
{
    public interface IRestaurantRepository : IRepository<Restaurant>
    {
    }
}
