﻿using System.Linq;

namespace Bauermediaau.Data
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll();
        T GetById(int id);
        void Add(T entity);
        void Update(T entity);
        void Delete(int id);
        void Delete(T entity);
        void Detach(T entity);
        void SaveChanges();
    }
}
