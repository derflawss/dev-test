﻿using System.Linq;
using System.Web.Mvc;
using Business;

namespace MvcApplication.Controllers
{
    public class HomeController : Controller
    {
        private IRestaurantService _service;

        public HomeController() :this(new RestaurantService()) { }

        public HomeController(IRestaurantService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            ViewBag.Restaurants = _service.GetAll().ToList();

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
