﻿using System;
using System.Web.Mvc;
using BauerMediaau.Model;
using Business;
using MvcApplication.ViewModel;

namespace MvcApplication.Controllers
{
    public class RestaurantController : Controller
    {
        private IRestaurantService _service;

        public RestaurantController() : this(new RestaurantService()) { }

        public RestaurantController(IRestaurantService service)
        {
            _service = service;

            AutoMapper.Mapper.CreateMap<Restaurant, RestaurantViewModel>();
        }

        //
        // GET: /Restaurant/
        public ActionResult Edit(int id)
        {

            var ret = _service.GetById(id);

            var viewModel = AutoMapper.Mapper.Map<RestaurantViewModel>(ret);


            return View("EditWithModel", viewModel);

        }

        [HttpPost]
        public ActionResult Edit(int id, RestaurantViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var exist = _service.GetById(id);

                if (exist != null)
                {
                    exist.Title = vm.Title;
                    exist.PhoneNumberText = vm.PhoneNumberText;

                    _service.Update(exist);

                    ViewBag.Message = String.Format("Saved '{0}'.", exist.Title);
                }
                else ViewBag.Message = String.Format("Invalid Restaurant '{0}'.", vm.Title);
                return View("EditWithModel", vm);
            }

            return View("EditWithModel");
        }
    }
}
