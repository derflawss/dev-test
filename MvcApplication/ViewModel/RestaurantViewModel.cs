﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MvcApplication.ViewModel
{
    public class RestaurantViewModel
    {
        [DisplayName("Phone Number")]
        [DataType(DataType.PhoneNumber)]
        [MinLength(9, ErrorMessage = "Phone number needs to be at least 9 digits in length")]
        public string PhoneNumberText { get; set; }

        public int Id { get; set; }

        [DisplayName("Title")]
        public string Title { get; set; }
    }
}