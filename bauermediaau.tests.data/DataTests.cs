﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bauermediaau.Data;

namespace Bauermediaau.Tests.Data
{
    [TestClass]
    public class DataTests
    {
        [TestMethod]
        public void GetAllRestaurantsShouldReturnEnumerableofRestaurantObjects ()
        {
            //Arrange
            var restaurantRepository = new RestaurantRepository(new DatabaseEntities());

            //Act
            var restaurants = restaurantRepository.GetAll();

            //Assert
            Assert.IsInstanceOfType(restaurants, typeof(IEnumerable<Restaurant>));
        }

        [TestMethod]
        public void GetRestaurantShouldReturnValidRestaurantObject()
        {
            //Arrange
            var restaurantRepository = new RestaurantRepository(new DatabaseEntities());

            var expected = restaurantRepository.GetAll().First();

            //Act
            var actual = restaurantRepository.GetById(expected.Id);

            //Assert
            Assert.AreEqual(expected.Id, actual.Id);
        }


        [TestMethod]
        public void UpdateRestaurantShouldPersistChanges()
        {
            //Arrange
            var repository = new RestaurantRepository(new DatabaseEntities());

            var expected = repository.GetAll().First();

            expected.HeadChefName = "Updated";

            //Act
            repository.Update(expected);

            //Assert
            var actual = repository.GetById(expected.Id);
            Assert.IsTrue(actual.HeadChefName.Equals("Updated"));
        }
    }
}
